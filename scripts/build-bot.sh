#!/usr/bin/env bash

set -e

TARGET=astropath/bot/cmd
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

VERSION=$("$DIR"/parseVersion.sh)

LDFLAGS="-X main.version=${VERSION} -s -w"

set +e

upxBin="$(command -v upx)"

echo "Making directories...."

set -e

mkdir -p bin/bot

echo "Building bot version ${VERSION}..."

OUTFILE="bin/bot/astropath"
GOOS=${GOOS:-"linux"} GOARCH="amd64" go build -ldflags "${LDFLAGS}" -o "$OUTFILE" $TARGET

if [ -n "$upxBin" ]; then
  upx -q "${OUTFILE}"
fi
