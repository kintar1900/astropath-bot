// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package commands

import (
	"fmt"
	"github.com/Kintar/dgc"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/kintar1900/astropath-api/datastore"
	"log"
)

func Beacons() *dgc.Command {
	return &dgc.Command{
		Name: "beacons",
		IgnoreCase:  true,
		Usage:       "beacons",
		Description: "Lists all tracked commanders and their last check-in time",
		Handler:     CheckinsHandler,
	}
}

func CheckinsHandler(ctx *dgc.Ctx) {
	repo := ctx.CustomObjects.MustGet("repo").(datastore.Repository)
	cmdrs, err := repo.All(datastore.EntryCommanderLocation)

	if err != nil {
		err = ctx.RespondText(fmt.Sprintf("I seem to have a problem reading from my data store. %v", err))
	} else {
		fields := make([]*discordgo.MessageEmbedField, len(cmdrs))
		for i, cmdr := range cmdrs {
			actual := cmdr.(*datastore.CommanderLocation)
			fields[i] = &discordgo.MessageEmbedField{
				Name:  actual.Name,
				Value: actual.LastBeacon.Format("2006-01-02 15:04 MST"),
			}
		}

		if err = ctx.RespondEmbed(&discordgo.MessageEmbed{
			Title: fmt.Sprintf("Beacon Report"),
			Fields: fields,
		}); err != nil {
			log.Println(err)
		}
	}
}
