// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package bot

import (
	"errors"
	"fmt"
	"github.com/Kintar/dgc"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/kintar1900/astropath-api/datastore/dynamodb"
	"gitlab.com/kintar1900/astropath-bot/commands"
	"log"
	"os"
	"os/signal"
	"reflect"
	"strings"
	"syscall"
	"time"
)

type botConfig struct {
	DataTableName string `env:"API_TABLE_NAME"`
	DiscordToken  string `env:"DISCORD_BOT_TOKEN"`
}

type MetricData struct {
	version   string
	startedAt time.Time
}

func populate(cfg *botConfig) error {
	v := reflect.ValueOf(cfg)
	if !v.Elem().CanAddr() {
		return errors.New("must pass a pointer to populate")
	}

	t := reflect.TypeOf(cfg)
	missingVars := make([]string, 0)

	for f := 0; f < t.Elem().NumField(); f++ {
		fld := t.Elem().Field(f)
		envVar := fld.Tag.Get("env")
		val, ok := os.LookupEnv(envVar)
		if !ok {
			missingVars = append(missingVars, envVar)
		} else {
			v.Elem().Field(f).SetString(val)
		}
	}

	if len(missingVars) > 0 {
		return errors.New(fmt.Sprint("can't start: missing required environment variables: ", strings.Join(missingVars, ", ")))
	}

	if strings.Index("Bot ", cfg.DiscordToken) != 0 {
		cfg.DiscordToken = "Bot " + cfg.DiscordToken
	}

	return nil
}

func Run(version string) {
	cfg := botConfig{}
	if err := populate(&cfg); err != nil {
		log.Fatalln(err)
	}

	log.Println(cfg.DiscordToken)

	session, _ := discordgo.New()
	session.Token = cfg.DiscordToken

	if err := startDiscord(session); err != nil {
		log.Fatalln(err)
	}

	defer func() { _ = session.Close() }()

	printWelcomeMessage(version)
	startHandlers(session, version)

	sChan := make(chan os.Signal, 1)
	log.Println("Press CTRL+C to exit...")
	signal.Notify(sChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Kill, os.Interrupt)
	_ = <-sChan
}

func printWelcomeMessage(version string) {
	fmt.Printf(`
  _____            __                              __  .__
 /  _  \   _______/  |________  ____ ___________ _/  |_|  |__
/  /_\  \ /  ___/\   __\_  __ \/  _ \\____ \__  \\   __\  |  \
/    |    \\___ \  |  |  |  | \(  <_> )  |_> > __ \|  | |   Y  \
\____|__  /____  > |__|  |__|   \____/|   __(____  /__| |___|  /
	   \/     \/                     |__|       \/          \/`)
	fmt.Printf("\n%64s\n\n", version)
}

func startDiscord(session *discordgo.Session) error {
	err := session.Open()
	if err != nil {
		return fmt.Errorf("failed to connect to discord: %w", err)
	}
	return nil
}

func startHandlers(session *discordgo.Session, version string) {

	repo, err := dynamodb.NewRepository()
	if err != nil {
		log.Fatalln("could not create dynamoDB connection: ", err)
	}

	router := dgc.Create(&dgc.Router{
		Prefixes: []string{
			"!",
			"astro!",
		},

		IgnorePrefixCase: true,

		BotsAllowed: false,

		Commands:    []*dgc.Command{},
		Middlewares: []dgc.Middleware{},
		PingHandler: func(ctx *dgc.Ctx) {
			ctx.RespondText("I don't play ping pong.")
		},
	})

	router.RegisterDefaultHelpCommand(session, nil)

	metric := MetricData{
		version:   version,
		startedAt: time.Now(),
	}

	router.RegisterMiddleware(func(next dgc.ExecutionHandler) dgc.ExecutionHandler {
		return func(ctx *dgc.Ctx) {
			ctx.CustomObjects.Set("MetricData", &metric)
			ctx.CustomObjects.Set("repo", repo)
			next(ctx)
		}
	})

	router.RegisterCmd(CreateStatusCommand())
	router.RegisterCmd(commands.CommanderLocation())
	router.RegisterCmd(commands.JumpPlans())
	router.RegisterCmd(commands.Beacons())
	router.Initialize(session)
}

func CreateStatusCommand() *dgc.Command {
	return &dgc.Command{
		Name: "status",
		Aliases: []string{
			"stat",
		},
		Usage: "status",
		Example: "status",
		Description: "Prints version and various metrics information",
		IgnoreCase:  true,
		RateLimiter: dgc.NewRateLimiter(10*time.Second, time.Second, func(ctx *dgc.Ctx) {
			_ = ctx.RespondText("You're asking that too often.")
		}),
		Handler: HandleStatusCommand,
	}
}

func HandleStatusCommand(ctx *dgc.Ctx) {
	metrics := ctx.CustomObjects.MustGet("MetricData").(*MetricData)
	if metrics.version == "" {
		metrics.version = "Unknown"
	}

	fields := make([]*discordgo.MessageEmbedField, 0)
	fields = append(fields, &discordgo.MessageEmbedField{
		Name: "Version",
		Value: metrics.version,
	})

	uptime := time.Since(metrics.startedAt)
	uptimeStr := ""
	if uptime.Seconds() < 60 {
		uptimeStr = fmt.Sprintf("%.0f seconds", uptime.Seconds())
	} else if uptime.Minutes() < 30 {
		uptimeStr = fmt.Sprintf("%.2f minutes", uptime.Minutes())
	} else {
		uptimeStr = fmt.Sprintf("%.2f hours", uptime.Hours())
	}

	embed := discordgo.MessageEmbed{
		Type:  discordgo.EmbedTypeRich,
		Title: "Astropath Status",
		Color: 0x03adfc,
		Footer: &discordgo.MessageEmbedFooter{
			Text: fmt.Sprintf("Status as of %v", time.Now().Format("2006-01-02 15:04:05 MST")),
		},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name: "Version",
				Value: metrics.version,
				Inline: true,
			},
			{
				Name: "Uptime",
				Value: uptimeStr,
				Inline: true,
			},
		},
	}

	if err := ctx.RespondEmbed(&embed); err != nil {
		log.Println(fmt.Sprintf("%v", err))
	}
}
