#!/usr/bin/env bash

set -e

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

. "${DIR}"/functions.sh

VERSION=$("$DIR"/parseVersion.sh)
BRANCH=${CI_COMMIT_BRANCH:-$(git rev-parse --abbrev-ref HEAD)}

echo -e "${YLW}Determined branch name '${BRANCH}'${NC}"

set +e
aws s3 mb s3://astropath-"${VERSION}"
set -e

s3bucket=s3://astropath-"${VERSION}"

aws s3 sync dist/ "$s3bucket"

set +e
set -x

aws cloudformation deploy --stack-name astropath-api-"$BRANCH" \
  --template-file aws/cloudformation/AstropathAPI.yaml \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameter-overrides VERSION="$VERSION" DeploymentStage="$BRANCH"

exitCode=$?

aws s3 rb --force "$s3bucket"

if [ $exitCode -eq 0 ] || [ $exitCode -eq 255 ]; then
  exit 0
else
  exit $exitCode
fi
