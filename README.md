# Astropath-bot

![current release](https://img.shields.io/badge/dynamic/json?color=blue&label=current%20release&query=%24.latestRelease&url=https%3A%2F%2Fgitlab.com%2Fkintar1900%2Fastropath-bot%2F-%2Fjobs%2Fartifacts%2Fmaster%2Fraw%2Fbadge-info.json%3Fjob%3DUnitTests&style=flat-square&logo=gitlab)

[![master version](https://img.shields.io/badge/dynamic/json?color=blue&label=master%20version&query=%24.masterVersion&url=https%3A%2F%2Fgitlab.com%2Fkintar1900%2Fastropath-bot%2F-%2Fjobs%2Fartifacts%2Fmaster%2Fraw%2Fbadge-info.json%3Fjob%3DUnitTests&style=flat-square&logo=gitlab)](https://gitlab.com/kintar1900/astropath-bot/) [![GitLab pipeline](https://img.shields.io/gitlab/pipeline/kintar1900/astropath-bot?style=flat-square&logo=gitlab)](https://gitlab.com/kintar1900/astropath-bot/builds)

## Overview

Astropath is a suite of tools for Elite Dangerous and Discord to allow squadrons to better coordinate and track the movements of their members.  It consists of an AWS-hosted API and data store ([here](https://gitlab.com/kintar1900/astropath-api)), a Windows "beacon" app that feeds ED Journal File information to the API ([found here](https://gitlab.com/kintar1900/astropath-beacon)), and a Discord bot to process queries into the data ( this repository ).

## Deployment

This repository contains a dockerfile.  Run `make build`, then build the docker file for a scratch-based linux container that will run the bot.  The bot requires the `API_DATA_TABLE` output from the API deployment, your credentials and region for AWS to be set in the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_DEFAULT_REGION` variables, and a valid discord bot token to be set in the `DISCORD_BOT_TOKEN` variable.