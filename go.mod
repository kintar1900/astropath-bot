module gitlab.com/kintar1900/astropath-bot

go 1.14

require (
	github.com/Kintar/dgc v1.0.7
	github.com/bwmarrin/discordgo v0.22.0
	gitlab.com/kintar1900/astropath-api v1.2.1
)
