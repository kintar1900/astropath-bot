#!/usr/bin/env bash

set -e

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
. "${DIR}/functions.sh"

# Calculate current tree version
version="$(build_version)"
echo "$version"