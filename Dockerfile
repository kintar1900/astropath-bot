FROM golang:1.14.6

WORKDIR /bot
COPY . .

RUN go mod download && go mod verify

RUN GOOS="linux" GOARCH="amd64" go build -o /bot/astropath-bot -ldflags "-s -w -X main.version=$(git describe --tags --dirty)" cmd/main.go

ENTRYPOINT [ "/bot/astropath-bot" ]