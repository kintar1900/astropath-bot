// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package commands

import (
	"fmt"
	"github.com/Kintar/dgc"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/kintar1900/astropath-api/datastore"
	"log"
	"strings"
	"time"
)

func JumpPlans() *dgc.Command {
	return &dgc.Command{
		Name: "carriers",
		Aliases: []string{
			"fc",
			"jumps",
		},
		Description: "Lists all known upcoming fleet carrier jumps",
		IgnoreCase:  true,
		Handler:     Handle,
	}
}

func Handle(ctx *dgc.Ctx) {
	repo := ctx.CustomObjects.MustGet("repo").(datastore.Repository)

	plans, err := repo.All(datastore.EntryJumpPlan)
	if err != nil {
		log.Println(err)
		_ = ctx.RespondText("Error talking to the data store.")
		return
	}

	if len(plans) == 0 {
		_ = ctx.RespondText("No current jumps scheduled.")
		return
	}


	embed := &discordgo.MessageEmbed{
		Type:  discordgo.EmbedTypeRich,
		Title: "Upcoming Carrier Jumps",
		Color: 0x009999,
		Footer: &discordgo.MessageEmbedFooter{
			Text: fmt.Sprintf("Plans reported at %s", time.Now().Format("2006-01-02 15:04:05 MST")),
		},
	}

	fields := make([]*discordgo.MessageEmbedField, 0)

	for _, plan := range plans {
		actual := plan.(*datastore.JumpPlan)
		fields = append(fields, &discordgo.MessageEmbedField{
			Inline: false,
			Name: actual.CarrierName,
			Value: fmt.Sprintf("Jumping from %s to %s at %s",
				actual.FromSystem.Name,
				actual.ToSystem.Name,
				actual.DepartingAt.Format("15:04:05 MST")),
		})
	}

	embed.Fields = fields

	if err := ctx.RespondEmbed(embed); err != nil {
		log.Println(err)
	}
}

func formatReportLine(now time.Time, plan *datastore.JumpPlan) string {
	departure := plan.DepartingAt
	etd := strings.TrimRight(fmt.Sprintf("%.2f", departure.Sub(now).Minutes()), "0")
	etd = strings.TrimRight(etd, ".")

	return fmt.Sprintf("%s: %s -> %s in %s minutes",
		plan.CarrierName,
		plan.FromSystem.Name,
		plan.ToSystem.Name,
		etd)
}
