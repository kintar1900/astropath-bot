// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package commands

import (
	"fmt"
	"github.com/Kintar/dgc"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/kintar1900/astropath-api/datastore"
	"log"
	"strings"
)

func CommanderLocation() *dgc.Command {
	return &dgc.Command{
		Name: "commander",
		Aliases: []string{
			"cmdr",
		},
		IgnoreCase:  true,
		Usage:       "commander <in-game name>",
		Description: "Shows the latest known location for a commander",
		Handler:     Handler,
	}
}

func Handler(ctx *dgc.Ctx) {
	cmdrName := ctx.Arguments.Get(0).Raw()
	repo := ctx.CustomObjects.MustGet("repo").(datastore.Repository)
	cmdr, err := repo.Get(strings.ToUpper(cmdrName), datastore.EntryCommanderLocation)

	if err != nil {
		err = ctx.RespondText(fmt.Sprintf("I seem to have a problem reading from my data store. %v", err))
	} else {
		actual, ok := cmdr.(*datastore.CommanderLocation)
		if !ok {
			err = ctx.RespondText("My apologies, but I was unable to locate that commander.")
		} else {
			err = ctx.RespondEmbed(&discordgo.MessageEmbed{
				Title: fmt.Sprintf("Location Report : Commander %s", actual.Name),
				Fields: []*discordgo.MessageEmbedField{
					{
						Name:  "Last Seen",
						Value: actual.LastBeacon.Format("2006-01-02 15:04:05 MST"),
					},
					{
						Name:  "Current System",
						Value: actual.CurrentSystem.Name,
					},
				},
			})
		}
	}

	if err != nil {
		log.Println(err)
		_ = ctx.RespondText("I am a little teapot... O.o   No, wait.  Something went wrong.")
	}
}
